import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import Header from './Header';
import Main from './Main';

class Home extends React.Component {
    render() {
        return (
            <div className="container">
                <Header />
            </div>
        );
    }
}

export default Home;
