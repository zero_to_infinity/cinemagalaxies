import React from 'react';

const Heading = () =>
    <div className="header">
        <div className="container">
            <div className="logo inline">Cinemagalaxies</div>
            <div className="menu inline">
                <div className="inline">
                    <div className="menu__item">Movies</div>
                </div>
                <div className="inline">
                    <div className="menu__item">Actors</div>
                </div>
            </div>
        </div>
    </div>;

export default Heading;
