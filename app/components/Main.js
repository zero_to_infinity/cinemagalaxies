import React from 'react';

const Heading = () =>
    <div className="main">
        <div className="container">
            <div className="logo inline">Cinemagalaxies</div>
            <div className="menu inline">
                <div className="inline menu__item">Movies</div>
                <div className="inline menu__item">Actors</div>
            </div>
        </div>
    </div>;

export default Heading;
